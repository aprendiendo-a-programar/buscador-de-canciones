import * as UI from './interfaz.js';
import API from './api.js';

UI.formularioBuscar.addEventListener('submit', buscarCacion);

function buscarCacion(e) {
    e.preventDefault();

    // Obtener datos del formulario
    const artista = document.querySelector('#artista').value;
    const cancion = document.querySelector('#cancion').value;
    
    // Validar si el usuario introduce información en los inputs
    if (artista === '' || cancion == '') {
        // Mostrar error cuando deje el campo vacio
        UI.divMensajes.textContent = "Error... Todos los campos son obligatorios";
        UI.divMensajes.classList.add('error');
 
        setTimeout(() => {
            UI.divMensajes.textContent = '';
            UI.divMensajes.classList.remove('error');
        }, 3000);

        return;
    }

    // Consultar la api
    const busqueda = new API(artista, cancion);
    busqueda.consultaApi();
}